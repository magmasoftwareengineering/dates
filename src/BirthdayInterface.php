<?php
/**
 * Created by PhpStorm.
 * User: jeremy
 * Date: 14/02/2016
 * Time: 22:54
 */

namespace MagmaSoftwareEngineering\Dates;

/**
 * Interface BirthdayInterface
 * @package MagmaSoftwareEngineering\Dates
 */
interface BirthdayInterface extends DatesInterface
{

    /**
     * @param string $date
     *
     * @return boolean
     */
    public function isBirthday($date);

    /**
     * @param $date
     *
     * @return mixed
     */
    public function birthdayDetails($date);
}
