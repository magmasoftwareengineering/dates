<?php
/**
 * Created by PhpStorm.
 * User: jeremy
 * Date: 14/02/2016
 * Time: 22:56
 */

namespace MagmaSoftwareEngineering\Dates;

/**
 * Class Birthday
 * @package MagmaSoftwareEngineering\Dates
 */
class Birthdays extends Dates implements BirthdayInterface
{

    public function __construct(array $birthdays = [])
    {

        if (0 !== count($birthdays)) {
            $this->setDates($birthdays);
        }
    }

    /**
     * @param string $date
     *
     * @return boolean
     */
    public function isBirthday($date)
    {

        return parent::isRequestedDate($date, 'm-d');
    }

    /**
     * @param $date
     *
     * @return array
     */
    public function birthdayDetails($date)
    {

        return parent::dateDetails($date, 'm-d');
    }
}
