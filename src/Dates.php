<?php
/**
 * Created by PhpStorm.
 * User: jeremy
 * Date: 14/02/2016
 * Time: 22:57
 */

namespace MagmaSoftwareEngineering\Dates;

/**
 * Class Dates
 * @package MagmaSoftwareEngineering\Dates
 */
class Dates
{

    /**
     * @var null
     */
    protected $timezone;

    /**
     * @var string
     */
    protected $datePregFormat = '`^[0-9]{4}-[0-9]{2}-[0-9]{2}$`';

    /**
     * @var string
     */
    protected $dateStringFormat = 'Y-m-d';

    /**
     * @var array
     */
    protected $dates = [];

    /**
     * @param string $timezone
     *
     * @return $this
     */
    public function setTimeZone($timezone = 'Europe/London')
    {

        $this->timezone = new \DateTimeZone($timezone);

        return $this;
    }

    /**
     * @param array $dates
     *
     * @return $this
     */
    public function setDates(array $dates = [])
    {

        $this->dates = $dates;

        return $this;
    }

    /**
     * @param string $year
     *
     * @return array
     */
    public function getDates($year = '')
    {

        $dates = $this->dates;

        if ('' !== $year && preg_match('`^[0-9]{4}$`', $year)) {
            $dates = [];
            foreach ($this->dates as $date => $details) {
                $dateNew = new \DateTime($date, $this->timezone);
                if ((string)$year === $dateNew->format('Y')) {
                    $dates[$date] = $details;
                }
            }
        }

        return $dates;
    }

    /**
     * @param string $format
     *
     * @return $this
     */
    public function setDateStringFormat($format = 'Y-m-d')
    {

        $this->dateStringFormat = $format;

        $searches = ['`Y`', '`m`', '`d`'];
        $replaces = ['[0-9]{4}', '[0-9]{2}', '[0-9]{2}'];

        $this->datePregFormat = '`^' . preg_replace($searches, $replaces, $format) . '$`';

        return $this;
    }

    /**
     * @param        $date
     * @param string $compareFormat
     *
     * @return bool
     */
    protected function isRequestedDate($date, $compareFormat = 'Y-m-d')
    {

        $retval = false;

        $date = $this->validateDate($date);

        $dates = array_keys($this->dates);
        if (0 !== count($dates)) {
            foreach ($dates as $requestedDate) {
                $requestedDate = \DateTime::createFromFormat('Y-m-d', $requestedDate, $this->timezone);
                if ($requestedDate && $date->format($compareFormat) === $requestedDate->format($compareFormat)) {
                    $retval = true;
                    break;
                }
            }
        }

        return $retval;
    }

    /**
     * @param        $date
     * @param string $compareFormat
     *
     * @return array
     */
    protected function dateDetails($date, $compareFormat = 'Y-m-d')
    {

        $dates = [];

        if ($this->isRequestedDate($date, $compareFormat)) {

            $date = $this->validateDate($date);

            foreach ($this->dates as $eventDate => $details) {
                $eventDate = \DateTime::createFromFormat('Y-m-d', $eventDate, $this->timezone);
                if ($eventDate && $date->format($compareFormat) === $eventDate->format($compareFormat)) {
                    $dates[] = $details;
                }
            }
        }

        return $dates;
    }

    /**
     * @param string|\DateTime $date defaults to Y-m-d (YYYY-MM-DD) format
     *
     * @return \DateTime
     */
    protected function validateDate($date)
    {

        if (!$date instanceof \DateTime) {
            if (!preg_match($this->datePregFormat, $date)) {
                throw new \InvalidArgumentException('Passed parameter $date is not a valid date');
            }

            $date = \DateTime::createFromFormat($this->dateStringFormat, "$date", $this->timezone);
            if (!$date) {
                throw new \InvalidArgumentException('Passed parameter $date is not a valid date');
            }
        }

        return $date;
    }
}
