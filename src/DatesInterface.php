<?php
/**
 * Created by PhpStorm.
 * User: jeremy
 * Date: 14/02/2016
 * Time: 22:57
 */

namespace MagmaSoftwareEngineering\Dates;

/**
 * Interface DatesInterface
 * @package MagmaSoftwareEngineering\Dates
 */
interface DatesInterface
{

    /**
     * @param string $tzone
     *
     * @return mixed
     */
    public function setTimeZone($tzone);

    /**
     * @param array $dates
     *
     * @return mixed
     */
    public function setDates(array $dates = []);

    /**
     * @param string $year
     *
     * @return mixed
     */
    public function getDates($year = '');

    /**
     * @param string $format
     *
     * @return mixed
     */
    public function setDateStringFormat($format = 'Y-m-d');
}
