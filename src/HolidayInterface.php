<?php
/**
 * Created by PhpStorm.
 * User: jeremy
 * Date: 06/02/2016
 * Time: 13:00
 */

namespace MagmaSoftwareEngineering\Dates;


/**
 * Interface HolidayInterface
 * @package MagmaSoftwareEngineering\Dates
 */
interface HolidayInterface extends DatesInterface
{

    /**
     * @param string $date
     *
     * @return boolean
     */
    public function isHoliday($date);

    /**
     * @param $date
     *
     * @return mixed
     */
    public function holidayDetails($date);

    /**
     * @param string $date
     *
     * @return boolean
     */
    public function isWorkingDay($date);

    /**
     * @param string|\DateTime $date
     * @param integer          $interval
     *
     * @return string
     */
    public function getNextWorkingDay($date, $interval);
}
