<?php
/**
 * Created by PhpStorm.
 * User: jeremy
 * Date: 07/02/2016
 * Time: 19:48
 */

namespace MagmaSoftwareEngineering\Dates;

/**
 * Class Holiday
 * @package MagmaSoftwareEngineering\Dates
 */
class Holidays extends Dates implements HolidayInterface
{

    private $workingDays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'];

    /**
     * Holiday constructor.
     *
     * @param array $holidays
     */
    public function __construct(array $holidays = [])
    {

        if (0 !== count($holidays)) {
            $this->setDates($holidays);
        }
    }

    public function setWorkingDays(array $workingDays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'])
    {

        $this->workingDays = $workingDays;

        return $this;
    }

    /**
     * @param string|\DateTime $date
     *
     * @return bool
     */
    public function isHoliday($date)
    {
        return parent::isRequestedDate($date);
    }

    /**
     * @param $date
     *
     * @return array
     */
    public function holidayDetails($date)
    {

        return parent::dateDetails($date);
    }

    /**
     * @param string|\DateTime $date
     *
     * @return bool
     */
    public function isWorkingDay($date)
    {

        $date = parent::validateDate($date);

        // PHP Date 'l': Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday
        $dayOfWeek = $date->format('l');

        return in_array($dayOfWeek, $this->workingDays) && !$this->isHoliday($date);
    }

    /**
     * This function gives you the next working day based on the interval
     *
     * Based on http://php.net/manual/en/function.date.php#117847
     *
     * @param string|\DateTime $date
     * @param int              $interval working days after the supplied date
     *
     * @return string
     */
    public function getNextWorkingDay($date, $interval = 1)
    {

        $retval = '';

        $date = parent::validateDate($date);

        $orig_date = clone $date;

        $addDay = 0;

        while ($interval--) {
            while (true) {
                $addDay++;

                $date = clone $orig_date;
                $date->add(\DateInterval::createFromDateString("$addDay Days"));

                if ($this->isWorkingDay($date)) {
                    $retval = $date->format('Y-m-d');
                    break;
                }
            }
        }

        return $retval;
    }
}
